/*
// the following should be sourced from config.js before the index.js is loaded
const config = {
  words: [
    { label: "Name", answer: "answer-0", syllables: [ "Na", "me" ] },
    { label: "ist", answer: "answer-1", syllables: [ "ist" ] },
    // and so on
  ],
  title: "Bestehen die Worte aus 2 Silben?",
  description: "Klick auf die Symbole um ein Wort zu hören. Schiebe das Wort in die richtige Spalte unten.",
  next: "#this-link-does-not-lead-anywhere-yet",
  nextLabel: "Zur nächsten Übung",
  audioSource: "../audio/"
}
*/

// set config and $ as global vars for the linter
/* global config */
/* global $ */

// the following two variables are to be compatible with the final WP plugin implementation
const DigMitConfig = config
const digMitExerciseID = 1

class DraggableSyllables {
  constructor (id, config) {
    this.config = config
    this.id = id
    this.$container = $('#digmit-exercise-container-' + id)
  }

  init () {
    this.initialiseContainer()
    this.initialiseHeader(config)
    this.initialiseWords(config.words)
    this.initialiseTargets()
    $('#next-exercise-button span').text(config.nextLabel)
    $('#next-exercise-button').on('click', function () {
      window.location = config.next
    })
  }

  initialiseContainer () {
    $('<div class="header"><h2></h2></div>').appendTo(this.$container)
    $('<div class="description"></div>').appendTo(this.$container)
    $('<div class="source-field"></div>').appendTo(this.$container)

    const successMessage = `
      <div class="success-message" style="display: none">
        <p>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          Super!
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
          <i class="fa fa-star fa-spin"></i>
        </p>
        <div id="next-exercise-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $(successMessage).appendTo(this.$container)
    $('<div class="target-field answer-0"><h3></h3></div>').appendTo(this.$container)
    $('<div class="target-field answer-1"><h3></h3></div>').appendTo(this.$container)
  }

  initialiseHeader () {
    const config = this.config
    $('title').text(config.title)
    const $header = $('.header h2')
    if (config.titleAudio.enabled) {
      let $el = $('<i class="fa fa-volume-up"></i>')
      $el.on('click', { label: config.titleAudio.label, source: config.audioSource }, this.playAudio)
      $header.html(config.title + ' ')
      $header.append($el)
      $el = $('<span> <small>(' + config.titleAudio.label.toUpperCase() + ')</small>?</span>')
      $header.append($el)
    } else {
      $header.text(config.title)
    }
    $('.description').text(config.description)
  }

  // take all words provided by the words array, create elements for them
  // and place them in the source area
  initialiseWords () {
    const config = this.config
    const words = config.words
    // we want to randomise the order of the words, before we use them
    const unshuffled = words.slice(0, words.length)
    const shuffled = []
    while (unshuffled.length > 0) {
      const i = Math.floor(Math.random() * unshuffled.length)
      shuffled.push(unshuffled.splice(i, 1)[0])
    }
    // now we can add all words as draggables to toe source field
    shuffled.forEach(word => {
      const el = '<div class="audio-button" id="' + word.label + '"><i class="fa fa-volume-up"></i></div>'
      const $el = $(el)
      $el.appendTo($('.source-field'))
      $el.draggable({ revert: true })
      $el.on('click', { label: word.label, source: config.audioSource }, this.playAudio)
    })
  }

  // initialise all target fields as droppables and create labels
  initialiseTargets () {
    const config = this.config
    $('.target-field').droppable({
      accept: '.audio-button',
      hoverClass: 'drop-hover',
    }).on('drop', { self: this }, this.processDrop)

    for (let i = 0; i < 2; i++) {
      let html = config.answers[i].label
      if (config.answers[i].hasAudio) {
        html += ' <i class="fa fa-volume-up"></i>'
      }
      $('.answer-' + i + ' h3').html(html).on('click', { label: config.answers[i].label, source: config.audioSource }, this.playAudio)
    }
  }

  // play an audio file associated with a word
  playAudio (event) {
    const source = event.data.source
    const label = event.data.label
    const audio = new Audio(source + label + '.mp3')
    audio.play()
  }

  // when a word was dropped into one of the targets, we'll process it here:
  // 1. move it to the target area (right after the heading)
  // 2. check if the answer is correct and add corresponding class
  processDrop (event, ui) {
    const self = event.data.self
    const $target = $(event.target)
    const id = ui.draggable.attr('id')
    const config = self.config
    const word = config.words.find(word => word.label === id)

    // move the draggable to the new target area after the first child (header)
    ui.draggable.insertAfter($target.children()[0])

    // after inserting we also change the html to include the word with all syllables
    let html = ''
    word.syllables.forEach(syllable => {
      // if it is not the first syllable in the word we include a dot operatur
      if (html.length) { html += '&sdot;' }
      html += syllable
    })
    html += ' <i class="fa fa-volume-up"></i>'
    // and if correctly we'll add a checkmark in front, otherwise a cross
    if ($target.first().hasClass(word.answer)) {
      html = '<i class="fa fa-check correct"></i> ' + html
    } else {
      html = '<i class="fa fa-times incorrect"></i> ' + html
    }
    // now set the new html content
    ui.draggable.html(html)

    // check if source field is already empty and delete it
    // (if it was already deleted before, nothing happens here as empty
    //  jQuery objects always return empty jQuery objects in sub selection)
    const $sourceField = $('.source-field')
    if ($sourceField.first().children().length === 0) {
      $sourceField.remove()
    }

    // check if all items are already placed correctly and set visibility for
    // the success message accordingly
    if (self.allCorrect(config)) {
      $('.success-message').css('display', 'block')
      $('.success-message a').attr('href', config.next)
      // remove the checkmarks and add audio-button-solved class
      $('.audio-button i:first-child').remove()
      $('.audio-button').addClass('exercise-solved')
      // remove the draggable from the audio buttons
      $('.audio-button').draggable('destroy')
      // if a label was put in the title, highlight it in the words
      if (config.titleAudio.enabled) {
        const $fields = $('.audio-button')
        for (let i = 0; i < $fields.length; i++) {
          let html = $fields.eq(i).html()
          const splits = html.split(' ')
          // the second split element is our word (the first is an empty string
          // because the word is preceded by a space). we have to search now
          // for label and create a highlighted version if it
          const wordSplits = splits[1].split(config.titleAudio.label)
          const highlighted = wordSplits.reduce(
            (acc, val) => acc + '<span class="highlighted">' + config.titleAudio.label + '</span>' + val
          )
          splits[1] = highlighted
          // now put everything back together again
          html = splits.reduce((acc, val) => acc + ' ' + val)
          $fields.eq(i).html(html)
        }
      }
    } else {
      $('.success-message').css('display', 'none')
    }
  }

  // check if all fields have been placed in their final position
  // and return true or false accordingly
  allCorrect (config) {
    // if there are still items in the source field, we are not done
    if ($('.source-field').first().children().length !== 0) {
      return false
    }
    // now check the answer fields if they only contain items with right class
    // (so far we only have two fixed target fields, but this can be abstracted)
    let correct = true
    for (let i = 0; i <= 1; i++) {
      const $fields = $('.answer-' + i + ' .audio-button')
      for (let f = 0; f < $fields.length; f++) {
        const id = $fields.eq(f).attr('id')
        const word = config.words.find(word => word.label === id)
        if (word.answer !== 'answer-' + i) {
          correct = false
          break
        }
        if (!correct) break
      }
    }
    if (correct) return true
    return false
  }
}

// start initialisation as soon as the document is ready
$('document').ready(function () {
  const exercise = new DraggableSyllables(digMitExerciseID, DigMitConfig)
  exercise.init()
})
