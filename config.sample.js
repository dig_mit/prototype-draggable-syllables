/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "config" }] */

// config object with list of available words for this exercise
// in the integrated version this will be generated dynamically by the backend
const config = {
  words: [
    { label: 'Name', answer: 'answer-0', syllables: ['Na', 'me'] },
    { label: 'habe', answer: 'answer-0', syllables: ['ha', 'be'] },
    { label: 'Woche', answer: 'answer-0', syllables: ['Wo', 'che'] },
    { label: 'schwere', answer: 'answer-0', syllables: ['schwe', 're'] },
    { label: 'Arbeit', answer: 'answer-0', syllables: ['Ar', 'beit'] },
    { label: 'wenig', answer: 'answer-0', syllables: ['we', 'nig'] },
    { label: 'sieben', answer: 'answer-0', syllables: ['sie', 'ben'] },
    { label: 'Tage', answer: 'answer-0', syllables: ['Ta', 'ge'] },
    { label: 'Stunde', answer: 'answer-0', syllables: ['Stun', 'de'] },
    { label: 'Bruder', answer: 'answer-0', syllables: ['Bru', 'der'] },
    { label: 'ist', answer: 'answer-1', syllables: ['ist'] },
    { label: 'jetzt', answer: 'answer-1', syllables: ['jetzt'] },
    { label: 'Aktivist', answer: 'answer-1', syllables: ['Ak', 'ti', 'vist'] },
    { label: 'sehr', answer: 'answer-1', syllables: ['sehr'] },
    { label: 'gearbeitet', answer: 'answer-1', syllables: ['ge', 'ar', 'bei', 'tet'] },
    { label: 'sechs', answer: 'answer-1', syllables: ['sechs'] },
    { label: 'Uhr', answer: 'answer-1', syllables: ['Uhr'] },
    { label: 'Baustelle', answer: 'answer-1', syllables: ['Bau', 'stel', 'le'] },
    { label: 'zufrieden', answer: 'answer-1', syllables: ['zu', 'frie', 'den'] },
    { label: 'Lohn', answer: 'answer-1', syllables: ['Lohn'] },
  ],
  // the columns in which the words can be moved with labels and optional
  // playable audio buttons (currently there can only be 2 columns)
  answers: [
    { label: 'JA', hasAudio: false },
    { label: 'NEIN', hasAudio: true },
  ],
  // the title / header of the exercise
  title: 'Bestehen die Worte aus 2 Silben?',
  titleAudio: {
    enabled: false, // whether a playable audio sign should be added to the title
    label: 'something', // what should be played
  },
  // a sentence or short paragraph to describe the exercise
  description: 'Klick auf die Symbole um ein Wort zu hören. Schiebe das Wort in die richtige Spalte unten.',
  // link to the next exercise / page
  next: '#this-link-does-not-lead-anywhere-yet',
  // label that is shown as a link to the next exercise / page
  nextLabel: 'Zur nächsten Übung',
  // directory where audio files for words and syllables are stored
  audioSource: '../audio/',
}
